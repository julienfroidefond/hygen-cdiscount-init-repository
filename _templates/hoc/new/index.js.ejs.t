---
to: packages/core/src/hocs/<%= name %>/index.js
---
import { branch, compose, mapProps, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { omit, path } from 'ramda';
import { withFeatureFlipping } from '@cdiscount/feature-flipping';

const mapStateToProps = state => ({
  prop1: path(['prop1'], state), // must be a selector
});

const mapDispatchToProps = {};

const mapFeaturesToProps = ({ myFeature }) => ({
  isMyFeatureEnabled: myFeature,
});

const hasNotRenderConditions = props => {
  const { isMyFeatureEnabled } = props;

  return isMyFeatureEnabled;
};

/**
 * This hoc is used to ...
 *
 * @todo : change description
 *
 * @example
 * with<%= Name %>(MyComponent)
 */
const with<%= Name %> = compose(
  withFeatureFlipping(mapFeaturesToProps),
  connect(mapStateToProps, mapDispatchToProps),
  branch(hasNotRenderConditions, renderNothing),
  mapProps(omit(['aProp'])),
);

with<%= Name %>.displayName = "with<%= Name %>"

export default with<%= Name %>;