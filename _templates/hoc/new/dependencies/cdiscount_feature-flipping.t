---
inject: true
to: packages/core/package.json
after: peerDependencies
skip_if: cdiscount/feature-flipping
---
    "@cdiscount/feature-flipping": "^4.1.8",