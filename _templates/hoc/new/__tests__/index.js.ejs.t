---
to: packages/core/src/hocs/<%= name %>/__tests__/index.js
---
import { itRendersAllMutations } from '@cdiscount/jest-utils';
import { renderNothing } from "recompose";
import React from "react";

import with<%= Name %> from '../';

const mutations = [
  {
    name: 'without props',
    props: null,
  },
];

const getState = (myFeatureEnabled = false) => ({
  prop1: "testing my container",
  appStates: {
    featureFlipping: {
      enabledFeatures: {
        myFeature: myFeatureEnabled
      }
    }
  }
});

const MockedComponent = () => <div></div>
const MockedContainer = with<%= Name %>(MockedComponent);

describe(with<%= Name %>.name, () => {
  describe('without state', () => {
    itRendersAllMutations(MockedContainer, mutations, {
      context: {
        store: {
          getState: () => ({}),
          subscribe: Function.prototype,
          dispatch: Function.prototype,
        },
      },
      selector: MockedComponent,
    });
  });

  describe('with state', () => {
    itRendersAllMutations(MockedContainer, mutations, {
      context: {
        store: {
          getState: () => getState(false),
          subscribe: Function.prototype,
          dispatch: Function.prototype,
        },
      },
      selector: MockedComponent,
    });
  });

  describe("with state and feature myFeature activated", () => {
    itRendersAllMutations(MockedContainer, mutations, {
      context: {
        store: {
          getState: () => getState(true),
          subscribe: Function.prototype,
          dispatch: Function.prototype
        }
      },
      selector: renderNothing()
    });
  });
});