---
to: "<%= packages.includes('action-types') ? 'packages/action-types/README.md' : null %>"
---
# <%= name.toLowerCase() %>-action-types

A package for the action types for the <%= name.toLowerCase() %> domain.

See https://confluence.cdiscount.com/display/~romain.gaillard/Folder+Structure

## Installation

    yarn add @cdiscount/<%= name.toLowerCase() %>-action-types

## API

<!-- Generated by documentation.js. Update this documentation by updating the source code. -->
