---
to: "<%= packages.includes('ui') ? 'packages/ui/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-ui",
  "version": "1.0.0",
  "description": "UI package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-ui",
  "dependencies": {
    "prop-types": "^15.6.0"
  },
  "peerDependencies": {
    "react": "^16.8.6",
    "styled-components": "^5.0.0-beta.5"
  },
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
