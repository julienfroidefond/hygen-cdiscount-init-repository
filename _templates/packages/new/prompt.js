module.exports = [
  {
    type: "input",
    name: "name",
    message: "What is the name of your domain ?",
    default: "noDomain"
  },
  {
    name: "packages",
    type: "multiselect",
    message: "What package do you want to generate ? (space to select)",
    choices: [
      { name: "core", value: "core" },
      { name: "ui", value: "ui" },
      { name: "application-components", value: "application-components" },
      { name: "templates", value: "templates" },
      { name: "containers", value: "containers" },
      { name: "action-types", value: "action-types" }
    ]
  }
];
