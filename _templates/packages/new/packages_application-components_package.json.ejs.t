---
to: "<%= packages.includes('application-components') ? 'packages/application-components/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-application-components",
  "version": "1.0.0",
  "description": "Application components package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-application-components",
  "dependencies": {
    "prop-types": "^15.6.0"
  },
  "peerDependencies": {
    "react": "^16.8.6"
  },
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
