---
to: "<%= packages.includes('templates') ? 'packages/templates/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-templates",
  "version": "1.0.0",
  "description": "Templates package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-templates",
  "dependencies": {
    "prop-types": "^15.6.0"
  },
  "peerDependencies": {
    "react": "^16.8.6"
  },
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
