---
to: "<%= packages.includes('core') ? 'packages/core/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-core",
  "version": "1.0.0",
  "description": "Core package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-core",
  "dependencies": {
    "@cdiscount/<%= name.toLowerCase() %>-action-types": "^1.0.0",
    "prop-types": "^15.6.0"
  },
  "peerDependencies": {
    "react": "^16.8.6",
    "redux-saga": "0.16.2"
  },
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
