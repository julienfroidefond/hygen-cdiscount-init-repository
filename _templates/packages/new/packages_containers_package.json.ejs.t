---
to: "<%= packages.includes('containers') ? 'packages/containers/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-containers",
  "version": "1.0.0",
  "description": "Containers package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-containers",
  "dependencies": {
    "prop-types": "^15.6.0"
  },
  "peerDependencies": {
    "react": "^16.8.6",
    "react-redux": "^7.0.2"
  },
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
