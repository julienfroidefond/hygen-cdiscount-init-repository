---
to: "<%= packages.includes('action-types') ? 'packages/action-types/package.json' : null %>"
---
{
  "name": "@cdiscount/<%= name.toLowerCase() %>-action-types",
  "version": "1.0.0",
  "description": "Action types package of <%= name.toLowerCase() %> domain",
  "main": "build/index.js",
  "module": "build/module.js",
  "moduleName": "<%= name.toLowerCase() %>-action-types",
  "publishConfig": {
    "registry": "http://tfsdropserver:8082/repository/npm/"
  },
  "author": "Cdiscount",
  "license": "UNLICENSED"
}
