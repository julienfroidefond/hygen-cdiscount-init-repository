---
to: "<%= hasOnlyContainer ? null : 'packages/' + componentType + '/src/' + componentNameDasherized + '/__stories__/index.js' %>"
---
import React from 'react';

<% if(hasContainerAndComponent){ -%>
import <%= ComponentName %> from '../component';
<% } else { -%>
import <%= ComponentName %> from '../';
<% } -%>

export default {
  component: <%= ComponentName %>,
  stories: [
    {
      name: '<%= ComponentName %>',
      story: (
        <<%= ComponentName %> label="Top ventes" />
      ),
    },
  ],
};
