---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: cdiscount/feature-flipping
---
    "@cdiscount/feature-flipping": "^4.1.8",