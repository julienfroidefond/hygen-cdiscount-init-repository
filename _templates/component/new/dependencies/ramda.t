---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: ramda
---
    "ramda": "^0.26.1",