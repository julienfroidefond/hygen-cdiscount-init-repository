---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: react-redux
---
    "react-redux": "^7.1.1",