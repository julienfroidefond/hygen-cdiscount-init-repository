---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: styled-components
---
    "styled-components": "^5.0.0-beta.5",