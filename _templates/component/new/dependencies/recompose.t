---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: recompose
---
    "recompose": "^0.30.0",