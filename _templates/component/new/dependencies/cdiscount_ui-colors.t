---
inject: true
to: packages/<%= componentType %>/package.json
after: peerDependencies
skip_if: cdiscount/ui-colors
---
    "@cdiscount/ui-colors": "^4.1.0",