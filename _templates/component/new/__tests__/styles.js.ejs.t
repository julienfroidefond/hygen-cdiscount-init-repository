---
to: "<%= hasOnlyContainer ? null : 'packages/'+ componentType + '/src/' + componentNameDasherized + '/__tests__/styles.js' %>"
---
import { itRendersStyledCorrectly } from '@cdiscount/jest-utils';
import React from 'react';

import { Button } from '../styles';

describe(Button.name, () => {
  itRendersStyledCorrectly(
    <Button />,
    <Button label="Button">
      <p>children element</p>
    </Button>,
  );
});
