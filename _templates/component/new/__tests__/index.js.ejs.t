---
to: packages/<%= componentType %>/src/<%= componentNameDasherized %>/__tests__/index.js
---
<% if(hasContainerAndComponent){ -%>
<% if(containerCodeSample){ -%>
import { itRendersAllMutations } from '@cdiscount/jest-utils';
import { renderNothing } from "recompose";

import <%= ComponentName %> from '../component';
import <%= ComponentName %>Container from '../';

const mutations = [
  {
    name: 'without props',
    props: null,
  },
];

const getState = (myFeatureEnabled = false) => ({
  prop1: "testing my container",
  appStates: {
    featureFlipping: {
      enabledFeatures: {
        myFeature: myFeatureEnabled
      }
    }
  }
});

describe(<%= ComponentName %>Container.name, () => {
  describe('without state', () => {
    itRendersAllMutations(<%= ComponentName %>Container, mutations, {
      context: {
        store: {
          getState: () => ({}),
          subscribe: Function.prototype,
          dispatch: Function.prototype,
        },
      },
      selector: <%= ComponentName %>,
    });
  });

  describe('with state', () => {
    itRendersAllMutations(<%= ComponentName %>Container, mutations, {
      context: {
        store: {
          getState: () => getState(false),
          subscribe: Function.prototype,
          dispatch: Function.prototype,
        },
      },
      selector: <%= ComponentName %>,
    });
  });

  describe("with state and feature myFeature activated", () => {
    itRendersAllMutations(<%= ComponentName %>Container, mutations, {
      context: {
        store: {
          getState: () => getState(true),
          subscribe: Function.prototype,
          dispatch: Function.prototype
        }
      },
      selector: renderNothing()
    });
  });
});

<% } -%>
<% } else { -%>
import { itRendersAllMutations } from '@cdiscount/jest-utils';

import <%= ComponentName %> from '../';

const mutations = [
  {
    name: 'Without props',
    props: null,
  },
  {
    name: 'with all props',
    props: {
      label: 'Top ventes',
    },
  },
];

describe('<%= ComponentName %>', () => {
  itRendersAllMutations(<%= ComponentName %>, mutations);
});

<% } -%>