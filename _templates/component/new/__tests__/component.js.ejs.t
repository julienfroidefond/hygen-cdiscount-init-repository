---
to: "<%= hasContainerAndComponent && !hasOnlyContainer ? 'packages/' + componentType + '/src/' + componentNameDasherized + '/__tests__/component.js' : null %>"
---
import { itRendersAllMutations } from '@cdiscount/jest-utils';

import <%= ComponentName %> from '../component';

const mutations = [
  {
    name: 'Without props',
    props: null,
  },
  {
    name: 'with all props',
    props: {
      label: 'Top ventes',
    },
  },
];

describe('<%= ComponentName %>', () => {
  itRendersAllMutations(<%= ComponentName %>, mutations);
});
