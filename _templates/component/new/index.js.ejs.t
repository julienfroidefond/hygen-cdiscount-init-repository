---
to: packages/<%= componentType %>/src/<%= componentNameDasherized %>/index.js
---
<% if(hasContainerAndComponent){ -%>
<% if(containerCodeSample){ -%>
import { branch, compose, mapProps, renderNothing } from 'recompose';
import { connect } from 'react-redux';
import { omit, path } from 'ramda';
import { withFeatureFlipping } from '@cdiscount/feature-flipping';

import <%= ComponentName %> from './component';

const mapStateToProps = state => ({
  prop1: path(['prop1'], state), // must be a selector
});

const mapDispatchToProps = {};

const mapFeaturesToProps = ({ myFeature }) => ({
  isMyFeatureEnabled: myFeature,
});

const hasNotRenderConditions = props => {
  const { isMyFeatureEnabled } = props;

  return isMyFeatureEnabled;
};

/**
 * This container is used to ...
 *
 * @todo : change description
 *
 * @example
 * <<%= ComponentName %>Container />
 */
const <%= ComponentName %>Container = compose(
  withFeatureFlipping(mapFeaturesToProps),
  connect(mapStateToProps, mapDispatchToProps),
  branch(hasNotRenderConditions, renderNothing),
  mapProps(omit(['aProp'])),
)(<%= ComponentName %>);

export default <%= ComponentName %>Container;

<% } else { -%>
import <%= ComponentName %> from './component';

export default <%= ComponentName %>;
<% } -%>
<% } else { -%>
import { string } from 'prop-types';
import React from 'react';

import { Button } from './styles';

/**
 * This component is used to ...
 *
 * @todo : change description
 *
 * @param {Object} props
 * @param {string} [props.label = null] Title of button
 *
 * @example
 * <<%= ComponentName %> label="Top ventes" />
 */
const <%= ComponentName %> = ({ label = null }) => <Button>{label}</Button>;

<%= ComponentName %>.propTypes = {
  label: string,
};

export default <%= ComponentName %>;

<% } -%>