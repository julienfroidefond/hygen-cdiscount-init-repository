---
to: null
foo: <% ComponentName = h.changeCase.pascal(componentName) %>
foo2: <% hasContainerAndComponent = componentType !== 'ui' || componentType === 'containers' %>
foo3: <% componentNameDasherized = h.inflection.dasherize(h.inflection.underscore(componentName)) %>
foo4: <% hasOnlyContainer = componentType === 'containers' %>
---