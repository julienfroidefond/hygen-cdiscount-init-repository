---
to: "<%= hasOnlyContainer ? null : 'packages/'+ componentType + '/src/' + componentNameDasherized + '/styles.js' %>"
---
import { scienceBlue } from '@cdiscount/ui-colors';
import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const Button = styled.a({
  backgroundColor: scienceBlue,
  cursor: 'pointer',
  borderColor: 'transparent',
  padding: '0.1rem 0.3rem',
});
