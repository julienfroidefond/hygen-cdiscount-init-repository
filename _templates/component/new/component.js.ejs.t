---
to: "<%= hasContainerAndComponent && !hasOnlyContainer ? 'packages/' + componentType + '/src/' + componentNameDasherized + '/component.js' : null %>"
---
import { string } from 'prop-types';
import React from 'react';

import { Button } from './styles';

/**
 * This component is used to ...
 *
 * @todo : change description
 *
 * @param {Object} props
 * @param {string} [props.label = null] Title of button
 *
 * @example
 * <<%= ComponentName %> label="Top ventes" />
 */
const <%= ComponentName %> = ({ label = null }) => <Button>{label}</Button>;

<%= ComponentName %>.propTypes = {
  label: string,
};

export default <%= ComponentName %>;
