module.exports = [
  {
    name: "componentName",
    type: "input",
    message: "What is the name of your component ?",
    default: "sample"
  },
  {
    name: "containerCodeSample",
    type: "confirm",
    message: "Do you want a code sample on container ?",
    default: true
  },
  {
    name: "componentType",
    type: "select",
    message: "What is the type of this component ?",
    choices: [
      { name: "ui", value: "ui" },
      { name: "application-components", value: "application-components" },
      { name: "template", value: "template" },
      { name: "containers", value: "containers" }
    ]
  }
];
