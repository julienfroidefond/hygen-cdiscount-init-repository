---
to: scripts/jest-reporter.js
---
const fs = require('fs');
const path = require('path');
const jestJunitReporter = require('../node_modules/jest-junit-reporter');
const jestHtmlReporter = require('../node_modules/jest-html-reporter');

const reportPath = path.resolve(__dirname, '..', '__reports__');

if (!fs.existsSync(reportPath)) {
  fs.mkdirSync(reportPath);
}

process.env.TEST_REPORT_PATH = reportPath;

module.exports = function jestReporters(...args) {
  jestJunitReporter.apply(this, args);
  return jestHtmlReporter.apply(this, args);
};
