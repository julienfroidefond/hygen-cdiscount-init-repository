---
to: .storybook/helpers/generateStoriesFromMutations/index.js
---
import generateStoryFromMutations from '../generateStoryFromMutations';

export default (...mutations) => (
  mutations.map((mutation) => ({
    name: mutation.name,
    story: generateStoryFromMutations(mutation.story),
  }))
);
