---
to: .storybook/config.js
---
import React from 'react';
import path from 'path';
import { configure, storiesOf, setAddon } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import infoAddon from '@storybook/addon-info';
import withReadme from 'storybook-readme/with-readme';

setOptions({
  name: 'Cdiscount React <%= name.toLowerCase() %>',
  url: 'http://tfs:8080/tfs/DefaultCollection/Mobility/_git/<%= name.toLowerCase() %>',
  downPanelInRight: true,
});

setAddon(infoAddon);

const addStories = (story, componentName, stories) => {
  stories.forEach((mutation) => {
    const mutationName = mutation.name;

    if (!mutationName) {
      console.warn(`Component ${componentName} has a non named mutation, it will not be rendered`);
      return;
    }

    story.addWithInfo(mutationName, mutation.description || null, () => mutation.story);
  });
}

const loadStories = () => {
  const context = require.context('../packages', true, /__stories__\/index.js$/);

  context.keys().filter(componentPath => !componentPath.includes('node_modules')).forEach((componentPath) => {
    try {
      const stories = context(componentPath).default;
      const componentName = (stories.component && stories.component.name) || stories.componentName;

      if (!componentName) {
        throw new Error(`Component located at ${componentPath} does not have a name. Please add componentName property if it's a pure component`);
      }

      const story = storiesOf(componentName, module);

      if (stories.readme) {
        story.addDecorator(withReadme(stories.readme));
      }

      if (!stories.stories) {
        console.warn(`Component ${componentName} have no stories, it will be rendered without props`);
        story.add('with no props', () => React.createElement(stories.component));
      } else {
        addStories(story, componentName, stories.stories)
      }
        
    } catch (error) {
      console.error(error);
    }
  });
}

configure(loadStories, module);
