---
to: README.md
---
# <%= name.toLowerCase() %>

<%= name.toLowerCase() %> is a set of NodeJS libraries in use here at Cdiscount. These libraries are available on our private NPM registry.

See https://confluence.cdiscount.com/display/~romain.gaillard/Folder+Structure

---

## Development

You'll need [`yarn`](https://github.com/yarnpkg/yarn) >= 1.0.2 because we are dependent on `yarn workspaces`. See [here](#lerna) for more details.

```bash
$ git clone http://tfs:8080/tfs/DefaultCollection/Mobility/_git/<%= name.toLowerCase() %>
$ cd <%= name.toLowerCase() %>
$ yarn
$ ... do your changes ...
$ yarn run build && yarn run test
```

#### Testing

Develop the feature/fix, and don't forget to bump the package version in `package.json`.

Jest use .babelrc !

```bash
yarn run test
```

If you want to watch your test suite:

```bash
$ yarn run test --watch
```

Before adressing a PR you can make sure everything works correctly by running

```bash
$ yarn && yarn run build && yarn run test && yarn run lint
```

## Lerna

This project is managed with [Lerna](https://lernajs.io/). Lerna is a tool to organize multiple NodeJS projects inside the same repository.

Lerna also allows us to use [Yarn workspaces](https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/) to manage our dependencies. This implies a few things:

* DevDependencies should be included in top package.json
* There should be `node_modules` or `yarn.lock` in sub-packages
* There is a special syntax to manage sub-packages dependencies:
  ```bash
  $ yarn workspace @cdiscount/package_name add new_dependency
  $ yarn workspace @cdiscount/package_name remove old_dependency
  ```
  ## Contributions
  Contributions are welcome as long as you warn us beforehand and create a pull request.
  Each modified module must be bumped in the PR. New module versions will be deployed after PR approval and merge into master.

### Notes

##### On versioning

We follow the [semver](http://semver.org/) semantic.

##### On documentation

We rely on [jsdoc](http://usejsdoc.org/) to document our libraries.

You can update documentation after changes with this command:

```bash
$ yarn run docs
```

### Rules

* One PR per module.
* Follow linter rules.
* Follow [Cdiscount guidelines](http://a06mobiguidelines.cdbdx.biz:10325/).
* Changes must be tested and snapshoted.
* Documentation must be up to date
* You'll need 2 reviewers to approve your PR.
