---
to: scripts/summary.js
---
#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const packagesDir = path.resolve(__dirname, '../packages');

// eslint-disable-next-line no-console
console.log('Generating a SUMMARY.md for Gitbook...');

// eslint-disable-next-line fp/no-mutating-methods
const packages = fs
  .readdirSync(packagesDir)
  .map(pkg => path.basename(pkg))
  .sort();

const markdownList = packages.map(pkg => `* [${pkg}](packages/${pkg}/README.md)`).join('\n');

const summary = `
# Summary

${markdownList}
`;

fs.writeFile(path.join(__dirname, '../SUMMARY.md'), summary, () => {
  // eslint-disable-next-line no-console
  console.log('SUMMARY.md updated.');
});
