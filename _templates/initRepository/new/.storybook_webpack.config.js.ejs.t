---
to: .storybook/webpack.config.js
---
module.exports = {
  module: {
    rules: [{
      test: /\.md$/,
      loader: "raw-loader"
    }]
  }
};
