---
to: package.json
sh: yarn
---
{
  "description": "Libraries of the domain <%= name.toLowerCase() %>",
  "repository": "http://tfs.cdbdx.biz:8080/tfs/DefaultCollection/Mobility/_git/<%= name.toLowerCase() %>",
  "author": "CDISCOUNT",
  "license": "UNLICENSED",
  "devDependencies": {
    "@cdiscount/bff-site-api": "^12.46.0",
    "@cdiscount/config": "^2.0.11",
    "@cdiscount/corvus": "^1.7.2",
    "@cdiscount/defect": "^1.1.1",
    "@cdiscount/eslint-config-cdiscount-base": "*",
    "@cdiscount/eslint-config-cdiscount-react": "*",
    "@cdiscount/feature-flipping": "^4.1.8",
    "@cdiscount/jest-it-returns-all-mutations": "^3.0.2",
    "@cdiscount/jest-it-returns-correct-result": "^2.1.0",
    "@cdiscount/jest-it-should-match-sequence": "^2.0.0",
    "@cdiscount/jest-utils": "3.0.0",
    "@cdiscount/ramda-is-nil-or-empty": "^2.0.5",
    "@cdiscount/redux-entities-utils": "^1.1.0",
    "@cdiscount/ui-colors": "^4.1.0",
    "@storybook/addon-actions": "^3.2.13",
    "@storybook/addon-info": "^3.2.13",
    "@storybook/addon-links": "^3.2.13",
    "@storybook/addon-options": "^3.2.13",
    "@storybook/react": "^3.2.13",
    "babel-plugin-dynamic-import-node": "^2.3.0",
    "babel-plugin-external-helpers": "^6.22.0",
    "babel-plugin-transform-imports": "^1.5.1",
    "babel-preset-es2015": "^6.24.1",
    "babel-preset-react": "^6.24.1",
    "babel-preset-stage-0": "^6.24.1",
    "commitizen": "^2.9.6",
    "cz-conventional-changelog": "^2.0.0",
    "documentation": "^5.3.3",
    "eslint": "^4.9.0",
    "husky": "^0.14.3",
    "jest": "^21.2.1",
    "jest-html-reporter": "^2.4.2",
    "jest-junit-reporter": "^1.1.0",
    "jest-snapshot": "^21.2.1",
    "lerna": "^2.11.0",
    "lint-staged": "^7.2.0",
    "prettier": "^1.14.2",
    "prop-types": "^15.6.2",
    "ramda": "^0.26.1",
    "react": "16.8.6",
    "react-dom": "16.8.6",
    "react-redux": "^7.1.1",
    "recompose": "^0.30.0",
    "redux-saga": "0.16.2",
    "rollup": "1.15.1",
    "rollup-plugin-babel": "^3.0.2",
    "rollup-plugin-commonjs": "^8.2.4",
    "rollup-plugin-includepaths": "^0.2.3",
    "rollup-plugin-json": "^4.0.0",
    "storybook-readme": "^3.0.6",
    "styled-components": "^5.0.0-beta.5"
  },
  "scripts": {
    "bootstrap": "lerna bootstrap --concurrency 1",
    "build": "lerna exec -- node ../../scripts/build.js",
    "commit": "git-cz",
    "docs": "lerna exec  -- bash ../../node_modules/.bin/documentation readme --section=API src/",
    "format": "prettier --write \"packages/**/*.js\"",
    "lint": "eslint --cache .",
    "precommit": "lint-staged",
    "pretest": "npm run build",
    "publish": "lerna exec -- ../../scripts/publish.sh",
    "storybook:build": "build-storybook -o build",
    "storybook": "start-storybook -p 9001 -c .storybook",
    "test": "jest packages",
    "test:coverage": "jest packages --coverage",
    "test:watch": "jest packages --watch"
  },
  "config": {
    "commitizen": {
      "path": "./node_modules/cz-conventional-changelog"
    }
  },
  "jest": {
    "coverageReporters": [
      "lcov",
      "text-summary"
    ],
    "collectCoverageFrom": [
      "packages/*/src/**/*.js"
    ],
    "setupFiles": [
      "raf/polyfill",
      "<rootDir>/scripts/jest-shim.js"
    ],
    "testPathIgnorePatterns": [
      "test-set.js",
      "/node_modules/",
      "fixtures",
      "test-set"
    ]
  },
  "lint-staged": {
    "*.{js,jsx,json}": [
      "prettier --write",
      "git add"
    ]
  },
  "private": true,
  "workspaces": [
    "packages/*"
  ]
}
