---
to: scripts/jest-shim.js
---
global.requestAnimationFrame = callback => {
  setTimeout(callback, 0);
};
/*
Solves this bug :
TypeError: Cannot read property 'bind' of undefined at new RequestAnimationFrameDefinition
*/
global.cancelAnimationFrame = callback => {
  setTimeout(callback, 0);
};
