---
to: .storybook/helpers/generateStoryFromMutations/index.js
---
import React from 'react';

export default mutations => (
  mutations.map((mutation, idx) => (
    <details key={`${idx + 0}`} open={!idx ? true : false}>
      <summary style={{ color: '#4d4a5e', fontWeight: 'bold' }}>
        {mutation.name}
      </summary>
      <div>
        {mutation.story}
      </div>
    </details>
  ))
);
