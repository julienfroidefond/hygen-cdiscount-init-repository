---
to: scripts/publish.sh
---
#!/bin/bash

PKG_SCOPE=$(node -pe 'JSON.parse(process.argv[1]).name.split("/")[0]' "$(cat package.json)")
PKG_NAME=$(node -pe 'JSON.parse(process.argv[1]).name.split("/")[1]' "$(cat package.json)")
PKG_VERSION=$(node -pe 'JSON.parse(process.argv[1]).version' "$(cat package.json)")
NPM_REGISTRY=$(npm config get registry)

if ! curl --output /dev/null --fail --silent -r 0-0 $NPM_REGISTRY/$PKG_SCOPE/$PKG_NAME/-/$PKG_NAME-$PKG_VERSION.tgz; then
  echo "Publishing ${PKG_NAME}@${PKG_VERSION}"
  npm publish
fi;
