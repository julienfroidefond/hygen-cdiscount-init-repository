---
to: .eslintignore
---
node_modules
/packages/*/build
/packages/cdiscount-cli/templates
__coverage__
__reports__
