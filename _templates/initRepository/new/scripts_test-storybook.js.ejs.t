---
to: scripts/test-storybook.js
---
const path = require('path');
const puppeteer = require('puppeteer');

const { error } = console;

/**
 *
 * @param {Object} page the sorybook page to visit
 * @param {string} selector the clickable element of the page on the right panel div from storybook
 */
const clickOnSelector = async (page, selector) => {
	/* eslint-disable-next-line  */
	await page.evaluate(passedSelector => {
		const component = document.querySelectorAll(
			`div.Pane.vertical.Pane1 > div > div:nth-child(1) > div > div:nth-child(3) ${passedSelector}`,
		);
		const data = [...component];
		data.forEach(element => element.click());
	}, selector);
};

/**
 * Browse storybook page and scrape error
 */
const scrapeError = async () => {
	const browser = await puppeteer.launch({
		args: ['--no-sandbox', '--disable-setuid-sandbox'],
	});

	const page = await browser.newPage();

	page.on('pageerror', err => {
		error(`[PageError] Story-Book error occurred : ${err}. Check in a local environnement by launching storybook to have more information.`);
		process.exit(1);
	});

	page.on('console', msg => {
		if (msg.type() === 'error') {
			if (msg.text() === 'Failed to load resource: the server responded with a status of 404 (Not Found)')
				return;

			error(`[Console] Story-Book error occurred : ${msg.text()}. Check in a local environnement by launching storybook to have more information.`);
			process.exit(1);
		}
	});

	const pathStoryBook = path.join(__dirname, '/../storybook-static/index.html');

	await page.goto(`file://${pathStoryBook}`);

	await clickOnSelector(page, 'a');

	await clickOnSelector(page, 'div > ul > li > div > ul > li > div:nth-child(1) > div');

	await clickOnSelector(page, 'a');

	await page.waitFor(5000);

	await browser.close();
};

scrapeError();
