---
to: scripts/build.js
---
/* eslint-disable no-param-reassign */
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const includePaths = require('rollup-plugin-includepaths');
const json = require('rollup-plugin-json');
const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
// eslint-disable-next-line import/no-dynamic-require
const packageJson = require(path.resolve(process.cwd(), 'package.json'));

const { log, error } = console;
const { moduleName } = packageJson;
const packageName = packageJson.name.split('/')[1];
const packageVersion = packageJson.version;
const fullPackageName = `${packageName}@${packageVersion}`;

const transformImports = [
  'transform-imports',
  {
    '@cdiscount/ui-icons': {
      transform: importName =>
        `@cdiscount/ui-icons/build/es/${importName.replace(/^(.)/, $1 => $1.toLowerCase())}`,
      preventFullImport: true,
    },
  },
];
const rollupConfig = (entryPath = './src/index.js', experimentalCodeSplitting = false) =>
  rollup.rollup({
    input: entryPath,
    onwarn: Function.prototype,
    acorn: {
      allowHashBang: true,
    },
    plugins: [
      includePaths(),
      json(),
      babel({
        runtimeHelpers: true,
        babelrc: false,
        exclude: 'node_modules/**',
        presets: [['es2015', { modules: false }], 'react', 'stage-0'],
        plugins: experimentalCodeSplitting ? ['external-helpers', transformImports] : ['external-helpers'],
      }),
    ],
  });

const buildCommonJsBrowserModule = () =>
  rollupConfig('./src/index.browser.js').then(bundle =>
    bundle.write({
      format: 'es',
      name: moduleName,
      file: 'build/index.browser.js',
    }),
  );

const buildBrowserEsModule = () =>
  rollupConfig('./src/index.browser.js').then(bundle =>
    bundle.write({
      format: 'es',
      file: 'build/module.browser.js',
    }),
  );

const buildCommonJsModule = () => {
  log(`${fullPackageName}: building commonjs module as ${moduleName}`);

  return rollupConfig().then(bundle =>
    bundle.write({
      format: 'umd',
      name: moduleName,
      dir: 'build',
    }),
  );
};

const searchFileModule = (baseDir, dir, filelist = []) => {
  const directory = baseDir + dir;
  fs.readdirSync(directory).forEach(file => {
    if (fs.statSync(directory + file).isDirectory()) {
      filelist = searchFileModule(baseDir, `${dir}${file}/`, filelist);
    } else {
      filelist = fs.existsSync(`${directory}index.js`) ? [...filelist, dir] : filelist;
    }
  });
  return filelist;
};

const buildEsModule = () => {
  log(`${fullPackageName}: building es module`);

  if (fullPackageName.includes('ui-icons')) {
    const moduleFiles = searchFileModule('./src/icons/', '', []);

    moduleFiles.forEach(pathFile => {
      rollupConfig(`./src/icons/${pathFile}`, true).then(bundle => {
        const pathFileSplit = pathFile.split('/');

        const nameFileEs = pathFileSplit[pathFileSplit.length - 2];

        const camelCased = nameFileEs.replace(/-([a-z])/g, g => g[1].toUpperCase());

        return bundle.write({
          format: 'es',
          file: `build/es/${camelCased}.js`,
        });
      });
    });
  }
  return rollupConfig('./src/index.js', true).then(bundle =>
    bundle.write({
      format: 'es',
      file: 'build/module.js',
    }),
  );
};

const build = () =>
  Promise.all([buildCommonJsModule(), buildEsModule()]).catch(err => {
    throw new Error(`error while building commonjs module ${err}`);
  });

const preBuild = () =>
  packageJson.scripts && packageJson.scripts.prebuild
    ? new Promise((resolve, reject) => {
        log(`${fullPackageName}: 'prebuild' script found, running it before build`);
        exec(packageJson.scripts.prebuild, err => {
          if (error) {
            reject(new Error(`error while executing 'prebuild' script ${err}`));
          }

          resolve();
        }).stdout.pipe(process.stdout);
      })
    : Promise.resolve();

const buildBrowser = () =>
  packageJson.browser
    ? Promise.all([buildCommonJsBrowserModule(), buildBrowserEsModule()])
    : Promise.resolve();

const postBuild = () =>
  packageJson.scripts && packageJson.scripts.postbuild
    ? new Promise((resolve, reject) => {
        // eslint-disable-next-line no-console
        console.log(`${fullPackageName}: 'postbuild' script found, running it after build`);
        exec(packageJson.scripts.postbuild, errorr => {
          if (errorr) {
            // eslint-disable-next-line no-console
            reject(new Error(`error while executing 'postbuild' script ${errorr}`));
          }

          resolve();
        }).stdout.pipe(process.stdout);
      })
    : Promise.resolve();

exec('rm -r build', () =>
  preBuild()
    .then(build)
    .then(buildBrowser)
    .then(postBuild)
    .catch(err => {
      error(`${fullPackageName}: ${err}`);
      process.exit(1);
    }),
);
