module.exports = [
  {
    name: "name",
    type: "input",
    message: "What is the name of your hook ?",
    default: "sample"
  }
];
