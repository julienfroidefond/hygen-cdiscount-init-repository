---
to: "<%= generatedFiles.includes('sagas') ? 'packages/core/src/sagas/__tests__/' + actionNameDashed + '.js' : null %>"
---
<% if(isRequest){ -%>
import itShouldMatchSequence, { NO_PARAM } from '@cdiscount/jest-it-should-match-sequence';
import <%= actionName %> from '../<%= actionNameDashed %>';

const bffClient = {
  <%= actionName %>: () => undefined,
};

const <%= actionName %>ApiResponse = {
  data: {
    id: 70402,
    name: 'Generated'
  },
  status: 200,
};

const params = {};

describe('<%= actionName %>', () => {
  itShouldMatchSequence(
    'nominal case',
    () =>
      <%= actionName %>(params),
    [NO_PARAM, bffClient, <%= actionName %>ApiResponse],
  );

  itShouldMatchSequence(
    'error when getting <%= actionName %>',
    () =>
      <%= actionName %>(params),
    [NO_PARAM, bffClient, new Error('<%= actionName %> error')],
  );
});
<% } -%>
