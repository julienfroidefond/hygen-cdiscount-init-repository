---
to: "<%= generatedFiles.includes('sagas') && isRequest ? 'packages/core/src/sagas/index.js' : null %>"
inject: true
after: import { takeEvery } from 'redux-saga';
skip_if: import <%= actionName %>
---
<% if(hasAFetchOnNavigate){ -%>
import { NavigationActions } from 'react-navigation';
<% } -%>
import <%= actionName %> from './<%= actionNameDashed %>';
import { <%= actionNameUppercased %>_REQUEST } from '@cdiscount/<%= domainname %>-action-types';
<% if(hasAFetchOnNavigate){ -%>
import fetchOnNavigate<%= ActionName %> from './fetch-on-navigate/<%= actionNameDashed%>';
<% } -%>
