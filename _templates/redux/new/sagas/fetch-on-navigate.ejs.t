---
to: "<%= generatedFiles.includes('sagas') && hasAFetchOnNavigate ? 'packages/core/src/sagas/fetch-on-navigate/' + actionNameDashed + '.js' : null %>"
---
import { put } from 'redux-saga/effects';

import {
  <%= actionName %>,
} from "../../actions/<%= actionNameDashed %>";

/**
 * @todo Here insert description of the saga.
 * This saga is aimed to request <%= actionName %> for getting data at navigation on a route
 *
 * @param {object} action - The action passed to saga
 * @param {object} action.routeName - The name of the processed route
 *
 */
export default function* fetchOnNavigate({ routeName }) {
  if (routeName === 'PRODUCT') {
    yield put(<%= actionName %>());
  }
}
