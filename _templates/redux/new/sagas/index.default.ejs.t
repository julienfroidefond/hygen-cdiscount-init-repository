---
to: "<%= generatedFiles.includes('sagas') && isRequest ? 'packages/core/src/sagas/index.js' : null %>"
inject: true
after: export default
skip_if: <%= actionNameUppercased %>_REQUEST
---
  yield takeEvery(<%= actionNameUppercased %>_REQUEST, <%= actionName %>);
  <% if(hasAFetchOnNavigate){ -%>
yield takeEvery(NavigationActions.NAVIGATE, fetchOnNavigate<%= ActionName %>);
  <% } -%>
