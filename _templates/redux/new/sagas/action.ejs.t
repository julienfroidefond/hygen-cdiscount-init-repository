---
to: "<%= generatedFiles.includes('sagas') ? 'packages/core/src/sagas/' + actionNameDashed + '.js' : null %>"
---
<% if(isRequest){ -%>
import { put, call } from 'redux-saga/effects';
//import bffAppApi from '@cdiscount/bff-app-api';
import bffSiteApi from '@cdiscount/bff-site-api';
import config from '@cdiscount/config';
import Corvus from '@cdiscount/corvus';

import { <%= actionName %>Error, <%= actionName %>Success } from '../actions/<%= actionNameDashed %>';

/**
 * @todo Here insert description of the saga.
 * This saga is aimed to request <%= actionName %> for getting data. 
 * We will store in entities the result.
 *
 * TODO: agnosticism of sagas; sample:
 * const isNativeApp = yield select(path(["application", "system"])) === "native";
 * const client = isNativeApp ? bffAppApi(config.get("bff.url")) : bffSiteApi(config.get("bff.url"));
 *
 * @param {object} action - The action passed to saga
 * @param {object} action.params - all params passed to the request
 *
 */
export default function* <%= actionName %>({ params }) {
  try {
    const client = bffSiteApi(config.get("bff.url"));

    const { data, status } = yield call(client.<%= actionName %>, params);
    if(status !== 200){
      yield put(<%= actionName %>Error(data));
    }else{
      yield put(<%= actionName %>Success(data));
    }
  } catch (e) {
    Corvus.captureException(e);
    yield put(<%= actionName %>Error(e.toString()));
  }
}
<% } else { -%>
import { put } from 'redux-saga/effects';
import Corvus from '@cdiscount/corvus';

import { <%= actionName %> } from '../actions/<%= actionNameDashed %>';

/**
 * @todo Here insert description of the saga.
 */
export default function* <%= actionName %>() {
  try {
    yield put(<%= actionName %>());
  } catch (e) {
    Corvus.captureException(e);
  }
}
<% } -%>
