---
to: "<%= generatedFiles.includes('selectors') ? 'packages/core/src/selectors/__tests__/<%= entityNameDashed %>.js' : null %>"
---
import { 
    get<%= EntityName %>,
    get<%= EntityName %>ById,
    get<%= EntityName %>Ids,
} from '../<%= entityNameDashed %>';
import { rawState, selectedId } from '../../__tests__/fixtures/<%= actionNameDashed %>';

describe(get<%= EntityName %>.name, () => {
  it('returns <%= entityName %> all byIds data', () => {
    expect(get<%= ActionName %>(rawState)).toMatchSnapshot();
  });

  it('returns <%= entityName %> by a precide id', () => {
    expect(get<%= EntityName %>ById(rawState, selectedId)).toMatchSnapshot();
  });

  it('returns <%= entityName %> all Ids', () => {
    expect(get<%= EntityName %>Ids(rawState)).toMatchSnapshot();
  });
});
