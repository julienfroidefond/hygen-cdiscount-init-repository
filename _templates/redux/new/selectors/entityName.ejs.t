---
to: "<%= generatedFiles.includes('selectors') ? 'packages/core/src/selectors/<%= entityNameDashed %>.js' : null %>"
---
import { path } from 'ramda';

/**
 * Get all entries in the byId key
 *
 * @param {object} state - The global state
 *
 */
export const get<%= EntityName %> = (state) => path(['entities', '<%= entityName %>', 'byId'], state);

/**
 * Get an object by an id
 *
 * @param {object} state - The global state
 * @param {object} id - The id of the wanted object
 *
 */
export const get<%= EntityName %>ById = (state, id) => path(['entities', '<%= entityName %>', 'byId', id], state);

/**
 * Get all the ids of the stored instances of the entity
 *
 * @param {object} state - The global state
 *
 */
export const get<%= EntityName %>Ids = (state) => path(['entities', '<%= entityName %>', 'all'], state);

export default {
    get<%= EntityName %>,
    get<%= EntityName %>ById,
    get<%= EntityName %>Ids,
}