---
to: "<%= generatedFiles.includes('actions') ? 'packages/action-types/src/index.js' : null %>"
inject: true
skip_if: <%= actionNameUppercased %>
after: export default
---
<% if(isRequest){ -%>
    <%= actionNameUppercased %>_ERROR,
    <%= actionNameUppercased %>_REQUEST,
    <%= actionNameUppercased %>_SUCCESS,
<% } else { -%>
    <%= actionNameUppercased %>
<% } -%>
