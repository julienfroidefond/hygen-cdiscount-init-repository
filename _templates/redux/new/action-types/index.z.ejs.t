---
to: "<%= generatedFiles.includes('actions') ? 'packages/action-types/src/index.js' : null %>"
prepend: true
inject: true
skip_if: export const <%= actionNameUppercased %>
---
<% if(isRequest){ -%>
export const <%= actionNameUppercased %>_ERROR = '@<%= domainNameUppercased %>/<%= actionNameUppercased %>_ERROR';
export const <%= actionNameUppercased %>_REQUEST = '@<%= domainNameUppercased %>/<%= actionNameUppercased %>_REQUEST';
export const <%= actionNameUppercased %>_SUCCESS = '@<%= domainNameUppercased %>/<%= actionNameUppercased %>_SUCCESS';
<% } else { -%>
export const <%= actionNameUppercased %> = '<%= actionNameUppercased %>';
<% } -%>
