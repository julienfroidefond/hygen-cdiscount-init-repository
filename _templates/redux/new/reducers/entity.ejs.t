---
to: "<%= generatedFiles.includes('reducers') &&  isRequest ? 'packages/core/src/reducers/' + entityNameDashed + '.js' : null %>"
---
import { initEntities, registerEntries } from '@cdiscount/redux-entities-utils';
import { prop } from 'ramda';
import { <%= actionNameUppercased %>_SUCCESS } from '@cdiscount/<%= domainname %>-action-types';

/**
 * State Shape :
 *
 * {
 *     <%= entityName %>: {
 *      all: [1, 2, 3],
 *      byId: {
 *        1: {
 *          id:1,
 *          name: 'sample1'
 *        },
 *        2: {
 *          id:2,
 *          name: 'sample2'
 *        },
 *        3: {
 *          id:3,
 *          name: 'sample3'
 *        }
 *      }
 *    }
 * }
 */

const initialState = initEntities();

/**
 * In this reducer we register all the data fetched for the entity
 *
 * @param {object} state - The global state
 * @param {object} action - The action processed
 * @param {object} action.data - The data get from the server
 *
 */
const <%= actionName %>Success = (state = initialState, action) => {
  const { data } = action;

  return registerEntries(data, prop('id'), state);
};

const reducers = (state = {}, action) => {
  const { type } = action;
  
  switch (type) {
    case <%= actionNameUppercased %>_SUCCESS:
      return <%= actionName %>Success(state, action);

    default:
      return state;
  }
};

export default reducers;