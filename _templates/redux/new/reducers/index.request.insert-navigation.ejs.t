---
to: packages/core/src/reducers/index.js
inject: true
after: navigation
skip_if: navigation<%= ActionName %>,
---
<% if(isRequest && generatedFiles.includes('reducers')){ -%>
    <%= actionName %>: navigation<%= ActionName %>,<% } -%>