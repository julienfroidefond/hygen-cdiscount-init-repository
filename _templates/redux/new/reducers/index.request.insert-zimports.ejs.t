---
to: packages/core/src/reducers/index.js
inject: true
prepend: true
skip_if: <%= entityNameDashed %>
---
<% if(isRequest && generatedFiles.includes('reducers')){ -%>
import <%= entityName %> from './<%= entityNameDashed %>';
import navigation<%= ActionName %> from './navigation/<%= actionNameDashed %>';
<% } -%>