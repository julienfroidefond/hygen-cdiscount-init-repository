---
to: "<%= generatedFiles.includes('reducers') ? 'packages/core/src/reducers/__tests__/index.js' : null %>"
---
import <%= actionName %>Reducer from '..';

describe(<%= actionName %>Reducer.name, () => {
  it('should export reducers in formated way', () => expect(<%= actionName %>Reducer).toMatchSnapshot());
});
