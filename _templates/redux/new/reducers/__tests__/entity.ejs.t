---
to: "<%= generatedFiles.includes('reducers') ? 'packages/core/src/reducers/__tests__/' + entityNameDashed + '.js' : null %>"
---
import itReturnsAllMutations from '@cdiscount/jest-it-returns-all-mutations';

import { <%= actionName %>Success } from '../../actions/<%= actionNameDashed %>';
import { rawDatas } from '../../__tests__/fixtures/<%= actionNameDashed %>';
import <%= actionName %>Reducer from '../<%= entityNameDashed %>';

const mutations = [
  {
    name: 'returns default state with unknown action type',
    props: [undefined, { type: 'unknown' }],
  },
  {
    name: 'returns state with <%= actionName %> request error action',
    props: [undefined, <%= actionName %>Success(rawDatas)],
  },
];

describe(<%= actionName %>Reducer.name, () => {
  itReturnsAllMutations(<%= actionName %>Reducer, mutations);
});
