---
to: "<%= generatedFiles.includes('reducers') ? 'packages/core/src/reducers/__tests__/navigation/' + actionNameDashed + '.js' : null %>"
---
import itReturnsAllMutations from '@cdiscount/jest-it-returns-all-mutations';

import <%= actionName %>Reducer from '../../<%= entityNameDashed %>';
import {
  <%= actionName %>,
  <%= actionName %>Error,
} from '../../../actions/<%= actionNameDashed %>';

const mutations = [
  {
    name: 'returns default state with unknown action type',
    props: [undefined, { type: 'unknown' }],
  },
  {
    name: 'returns state with <%= actionName %> request in progress action',
    props: [undefined, <%= actionName %>()],
  },
  {
    name: 'returns state with <%= actionName %> request error action',
    props: [undefined, <%= actionName %>Error(new Error('test error'))],
  },
];

describe(<%= actionName %>Reducer.name, () => {
  itReturnsAllMutations(<%= actionName %>Reducer, mutations);
});
