---
to: "<%= !isRequest && generatedFiles.includes('reducers') ? 'packages/core/src/reducers/index.js' : null %>"
---
import <%= actionName %> from './navigation/<%= actionNameDashed %>';

/**
 * This object permits to declare all the keys of the store.
 * Only 4 keys are normed. The main are entities for your datas and navigation for your visualisations.
 * Cf guideline here : TODO !
 *
 */
export default {
  entities: null,
  navigation: { 
    <%= actionName %>,
  },
  session: null
};