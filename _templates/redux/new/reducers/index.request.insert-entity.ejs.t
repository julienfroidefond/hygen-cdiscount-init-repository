---
to: packages/core/src/reducers/index.js
inject: true
after: entities
skip_if: <%= entityName %>,
---
<% if(isRequest && generatedFiles.includes('reducers')) { -%>
    <%= entityName %>,<% } -%>