---
to: "<%= generatedFiles.includes('reducers') && isRequest ? 'packages/core/src/reducers/navigation/' + actionNameDashed + '.js' : null %>"
---
import {
  <%= actionNameUppercased %>_ERROR,
  <%= actionNameUppercased %>_REQUEST,
} from '@cdiscount/<%= domainname %>-action-types';
import { InvalidObject, LoadingObject } from '@cdiscount/defect';

const initialState = {};

/**
 * In this reducer we store an InvalidObject with error message
 *
 * @param {object} state - The global state
 * @param {object} action - The action processed
 * @param {object} action.error - The error message
 *
 */
const <%= actionName %>Error = (state, { error }) => InvalidObject({ error });

/**
 * In this reducer we add a loading state on the entity
 *
 * @param {object} state - The global state
 *
 */
const <%= actionName %>Request = (state) => LoadingObject(state);

const reducers = (state = initialState, action) => {
  const { type } = action;
  
  switch (type) {
    case <%= actionNameUppercased %>_REQUEST:
      return <%= actionName %>Request(state, action);

    case <%= actionNameUppercased %>_ERROR: {
      return <%= actionName %>Error(state, action);
    }

    default:
      return state;
  }
};

export default reducers;
