---
to: "<%= generatedFiles.includes('reducers') ? 'packages/core/src/reducers/index.js' : null %>"
---
/**
 * This object permits to declare all the keys of the store.
 * Only 4 keys are normed.
 * Cf guideline here : TODO !
 *
 */
export default {
  entities: {
  },
  navigation: {
  },
  session: null
};