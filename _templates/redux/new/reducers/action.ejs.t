---
to: "<%= generatedFiles.includes('reducers') && !isRequest ? 'packages/core/src/reducers/' + actionNameDashed + '.js' : null %>"
---
import {
    evolve,
} from 'ramda';
import {
  <%= actionNameUppercased %>,
} from '../actions/<%= actionNameDashed %>';

/**
 * State Shape :
 * {
 *    sampleActionData : {
 *      id: 1,
 *      name: "sample"
 *    }
 * }
 */

const initialState = {};

/**
 * In this reducer we register the data
 *
 * @param {object} state - The global state
 * @param {object} action - The action processed
 * @param {object} action.data - The data
 *
 */
const <%= actionName %> = (state, { data }) =>
  evolve(
    {
      data
    },
    state
  );

export default function(state = initialState, action) {
  const { type } = action;

  switch (action.type) {
    case <%= actionNameUppercased %>:
      return <%= actionName %>(state, action);

    default:
      return state;
  }
};
