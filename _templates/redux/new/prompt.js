module.exports = [
  {
    type: "input",
    name: "domainName",
    message: "What is the name of your domain ?",
    default: "noDomain"
  },
  {
    type: "confirm",
    name: "isRequest",
    message: "Is it a request ?",
    default: true
  },
  {
    type: "input",
    name: "actionName",
    message: "What's your base action ? (sample : getShowcase)",
    default: "sampleAction"
  },
  {
    type: "input",
    name: "entityName",
    message: "What's your entity name ? (Model of data name. sample : offer)",
    default: "sampleEntity"
  },
  {
    type: "confirm",
    name: "hasAFetchOnNavigate",
    message: "Do you want a fetch on navigate sample ?",
    default: false
  },
  {
    name: "generatedFiles",
    type: "multiselect",
    message: "What files do you want to generate ? (space to select)",
    choices: [
      { name: "actions", value: "actions" },
      { name: "sagas", value: "sagas" },
      { name: "reducers", value: "reducers" },
      { name: "selectors", value: "selectors" }
    ]
  }
];
