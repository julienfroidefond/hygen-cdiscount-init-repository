---
to: "<%= generatedFiles.includes('actions') ? 'packages/core/src/actions/' + actionNameDashed + '.js' : null %>"
---
<% if(isRequest){ -%>
import {
  <%= actionNameUppercased %>_ERROR,
  <%= actionNameUppercased %>_REQUEST,
  <%= actionNameUppercased %>_SUCCESS
} from "@cdiscount/<%= domainname %>-action-types";

/**
 * Request action.
 *
 * @param {object} params - all params passed to the request
 * @return {object} the action
 *
 * @example
 *
 *     yield put(<%= actionName %>({ param1: 'param1' }));
 */
export const <%= actionName %> = params => ({
  type: <%= actionNameUppercased %>_REQUEST,
  params,
});

/**
 * Success action.
 *
 * @param {object} data - all datas get from the request
 * @return {object} the action
 *
 * @example
 *
 *     yield put(<%= actionName %>Success({ id:1 }));
 */
export const <%= actionName %>Success = data => ({
  type: <%= actionNameUppercased %>_SUCCESS,
  data,
});

/**
 * Error action.
 *
 * @param {object} [error = null] - the error
 * @return {object} the action
 *
 * @example
 *
 *     yield put(<%= actionName %>Error("There is a problem"));
 */
export const <%= actionName %>Error = (error = null) => ({
  type: <%= actionNameUppercased %>_ERROR,
  error,
});

export default {
  <%= actionName %>,
  <%= actionName %>Success,
  <%= actionName %>Error,
};
<% } else { -%>
import {
  <%= actionNameUppercased %>,
} from "@cdiscount/<%= domainname %>-action-types";

/**
 * Request action.
 *
 * @param {object} data - params all datas passed to the request
 * @return {object} the action
 *
 * @example
 *
 *     yield put(<%= actionName %>({ param1: 'param1' }));
 */
export const <%= actionName %> = data => ({
  type: <%= actionNameUppercased %>,
  data
});

export default {
  <%= actionName %>,
};
<% } -%>