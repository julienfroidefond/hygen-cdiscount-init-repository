---
to: "<%= generatedFiles.includes('actions') ? 'packages/core/src/actions/__tests__/' + actionNameDashed + '.js' : null %>"
---
<% if(isRequest){ -%>
import itReturnsCorrectResult from '@cdiscount/jest-it-returns-correct-result';

import { <%= actionName %>, <%= actionName %>Error, <%= actionName %>Success } from '../../actions/<%= actionNameDashed %>';
import { rawDatas } from '../fixtures/<%= actionNameDashed %>';

describe(<%= actionName %>.name, () => {
  describe('returns <%= actionName %> action', () => {
    itReturnsCorrectResult(<%= actionName %>());
  });

  describe('returns <%= actionName %> result with request success action', () => {
    itReturnsCorrectResult(<%= actionName %>Success(rawDatas));
  });

  describe('returns <%= actionName %> result with request error action', () => {
    itReturnsCorrectResult(<%= actionName %>Error());
  });
});
<% } else { -%>
import itReturnsCorrectResult from '@cdiscount/jest-it-returns-correct-result';

import { <%= actionName %> } from '../actions/<%= actionNameDashed %>';

describe(<%= actionName %>.name, () => {
  describe('returns <%= actionName %> action', () => {
    itReturnsCorrectResult(<%= actionName %>());
  });
});
<% } -%>