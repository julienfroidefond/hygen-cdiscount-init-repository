---
inject: true
to: packages/core/package.json
after: peerDependencies
skip_if: redux-saga
---
    "redux-saga": "0.16.2",