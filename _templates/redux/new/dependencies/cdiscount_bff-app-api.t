---
inject: true
to: packages/core/package.json
after: peerDependencies
skip_if: cdiscount/bff-app-api
---
    "@cdiscount/bff-app-api": "^4.12.0",