---
inject: true
to: packages/core/package.json
after: peerDependencies
skip_if: cdiscount/bff-site-api
---
    "@cdiscount/bff-site-api": "^12.46.0",