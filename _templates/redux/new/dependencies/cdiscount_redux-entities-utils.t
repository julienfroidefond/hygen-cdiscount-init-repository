---
inject: true
to: packages/core/package.json
after: peerDependencies
skip_if: cdiscount/redux-entities-utils
---
    "@cdiscount/redux-entities-utils": "^1.1.0",