---
to: null
--- 
<% actionNameUppercased = h.inflection.underscore(actionName, true).toUpperCase() %>
<% domainNameUppercased = h.inflection.underscore(domainName, true).toUpperCase() %>
<% actionNameDashed = h.inflection.dasherize(h.inflection.underscore(actionName)) %>
<% ActionName = h.inflection.camelize(actionName) %>
<% entityNameDashed = h.inflection.dasherize(h.inflection.underscore(entityName)) %>
<% EntityName = h.inflection.camelize(entityName) %>
<% domainname = domainName.toLowerCase() %>