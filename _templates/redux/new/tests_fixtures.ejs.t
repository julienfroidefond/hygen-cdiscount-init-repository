---
to: "<%= isRequest ? 'packages/core/src/__tests__/fixtures/' + actionNameDashed + '.js' : null %>"
---
export const rawDatas = [
  {
    id: 1213,
    name: "name"
  }
];

export const rawState = {
  entities: {
    <%= entityName %>: {
      all: [1, 2, 3],
      byId: {
        1: {
          id: 1,
          name: "sample1"
        },
        2: {
          id: 2,
          name: "sample2"
        },
        3: {
          id: 3,
          name: "sample3"
        }
      }
    }
  }
};

export const selectedId = rawState.entities.<%= entityName %>.all[0];
