---
to: "<%= generatedFiles.includes('selectors') ? 'packages/core/src/index.js' : null %>"
inject: true
append: true
skip_if: selectors
---
export { default as selectors<%= EntityName %> } from 'selectors/<%= entityNameDashed %>';