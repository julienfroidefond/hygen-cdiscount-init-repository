---
to: packages/core/src/index.js
inject: true
append: true
skip_if: reducers/<%= entityNameDashed %>
---
<% if(generatedFiles.includes('reducers') && isRequest){ -%>
export { default as reducers } from 'reducers';
export { default as reducers<%= EntityName %> } from 'reducers/<%= entityNameDashed %>';
<% } -%>