---
to: packages/core/src/index.js
inject: true
append: true
skip_if: reducers/<%= actionNameDashed %>
---
<% if(generatedFiles.includes('reducers') && !isRequest){ -%>
export { default as reducers } from 'reducers';
export { default as reducers<%= ActionName %> } from 'reducers/<%= actionNameDashed %>';
<% } -%>