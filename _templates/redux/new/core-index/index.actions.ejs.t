---
to: packages/core/src/index.js
inject: true
append: true
skip_if: actions/<%= actionNameDashed %>
---
<% if(generatedFiles.includes('actions')){ -%>
export { default as actions} from 'actions/<%= actionNameDashed %>';
<% } -%>