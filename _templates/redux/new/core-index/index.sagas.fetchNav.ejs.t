---
to: packages/core/src/index.js
inject: true
append: true
skip_if: sagas/fetch-on-navigate/<%= actionNameDashed %>
---
<% if(generatedFiles.includes('sagas') && hasAFetchOnNavigate){ -%>
export { default as sagaFetchOnNavigate<%= ActionName %> } from 'sagas/fetch-on-navigate/<%= actionNameDashed %>';
<% } -%>