---
to: packages/core/src/index.js
inject: true
append: true
skip_if: sagas/<%= actionNameDashed %>
---
<% if(generatedFiles.includes('sagas')){ -%>
export { default as sagas<%= ActionName %> } from 'sagas/<%= actionNameDashed %>';
export { default as sagas } from 'sagas';
<% } -%>