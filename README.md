# hygen-cdiscount-init-repository

This is an hygen package that supercharges Cdiscount XP client workflow.

See https://confluence.cdiscount.com/display/~romain.gaillard/Folder+Structure

## Quick Start

```
$ npm i -g hygen
$ npm i -g hygen-add
$ mkdir your-app && cd your-app
$ yarn init
$ hygen-add https://gitlab.com/julienfroidefond/hygen-cdiscount-init-repository

$ hygen initRepository new
$ hygen packages new
$ hygen redux new
$ hygen component new
$ hygen hoc new
$ hygen hook new
$ hygen helper new
```

## About generators

### initRepository

This generator provide a simple way to generate a multi package repo in the Cdiscount way.

### packages

This generator generates package json and helps formatting the name of packages.

### redux

This generator generates actions reducers action types and sagas for your redux part.

### component

This generator generates components. It provides a way to show where to put things and a code sample for testing and containers.

### hoc

This generator generates a code sampled hoc with a test.

### hook

This generator generates a simple file and his test. No code sample.

### helper

This generator generates a simple file and his test. No code sample.

## Updating templates

TODO
